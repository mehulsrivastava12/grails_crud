package grails_crud.util

import java.text.SimpleDateFormat

class DateUtil {
    static SimpleDateFormat dd_mm_yyyy_hyphen = new SimpleDateFormat("dd/mm/yyyy")
    static SimpleDateFormat yyyy_mm_dd_hyphen = new SimpleDateFormat("yyyy-mm-dd")

    static String formatYearMonthDayToDayMonthYear(String dateString) {
        Date date = yyyy_mm_dd_hyphen.parse(dateString)
        return dd_mm_yyyy_hyphen.format(date)
    }
}
