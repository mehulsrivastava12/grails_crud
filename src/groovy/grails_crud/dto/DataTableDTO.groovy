package grails_crud.dto

class DataTableDTO<T> {

    T data
    Integer orderColumnIndex = 0
    Integer total = 0
    Integer filteredTotal = 0
    String direction = 'asc'
}