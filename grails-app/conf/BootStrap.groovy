import org.example.SecRole
class BootStrap {

    def init = { servletContext ->
        def userRole = SecRole.findByAuthority('ROLE_USER') ?: new SecRole(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = SecRole.findByAuthority('ROLE_ADMIN') ?: new SecRole(authority: 'ROLE_ADMIN').save(failOnError: true)
        def superUser = SecRole.findByAuthority('ROLE_SUPERUSER') ?: new SecRole(authority: 'ROLE_SUPERUSER').save(failOnError: true)

    }
    def destroy = {
    }
}
