package grails_crud

import grails.converters.JSON
import grails_crud.dto.DataTableDTO
import org.codehaus.groovy.grails.web.mapping.LinkGenerator
import org.example.SecUser

class UserController {
    UserService userService
    def mailService
    LinkGenerator grailsLinkGenerator

    def login(){
        render(view: "/user/_login")
    }

    def home() {
        render(view: '_home')
    }

    def loginOtp(){
        render(view: "/user/loginOtp")
    }

    def signup() {
        String template = userService.generateSignupPopup()
        def map = [form: template]
        render map as JSON
    }

    def index(){
        render(view: "/user/index")
    }

    def enterOTP(){
        render(view: 'OTPSubmit',model: [email:params.email])
    }

    def passwordChange(){
        render(view: 'forgotPassword',model: [email:params.email])
    }

    def create(){
        String password = params.password
        params.password = password.encodeAsBase64()
        params.otp = userService.generateOTP()
        def response = userService.saveData(params)
        if(response != null){
            def link = grailsLinkGenerator.link(controller: 'user', action: 'enterOTP', absolute: true,params: [email: params.email])
            mailService.sendMail {
                multipart true
                to params.email
                subject message(code: 'default.successfully.subject')
                html "Hi "+params.firstName +"<br>"+"${message(code: 'default.successfully.body')} <br>"+"${message(code: 'default.otp.login')}"+
                "<strong>${params.otp}<strong> <br> ${message(code: 'default.login.url')} " +
                        "<a href='${link}'>${message(code: 'default.click.here')} </a>."

            }
            flash.message = message(code: "default.registration.successful")
            render(view: 'OTPSubmit',model: [email: params.email])
        }
        else{
            redirect(controller: "user", action: "signupFailed")
        }
    }

    def authenticate(){
        String password = params.password
        params.password = password.encodeAsBase64()
        def user = userService.authentication(params)
        if(user == null){
            flash.message = message(code: "login.failed")
            redirect(controller: "user", action: "login")
        }
        else {
            redirect(controller: "user", action: "home")
        }
    }

    def getDataById() {
        def response = userService.getDataById(params.id)
        render(view: '_uploadFile', model: [detail: response])
    }

    def updateDataById() {
//        String password = params.password
//        params.password = password.encodeAsBase64()
        userService.updateDataById(params)
        redirect(controller: "user", action: "home")
    }

    def deleteUser() {
        userService.deleteUser(params)
        redirect(controller: "user", action: "home")
    }

    def fetchAllUser() {
        DataTableDTO<List<List<String>>> dataTableDTO =  userService.LoadData()
        render(contentType: 'text/json') {
            [
                    'draw'           : params.draw,
                    'recordsTotal'   : 0,
                    'recordsFiltered': 0,
                    'order'          : [[dataTableDTO.orderColumnIndex ?: 0, dataTableDTO.direction ?: 'asc']],
                    'data'           : dataTableDTO.data ?: []
            ]
        }
    }

    def signupFailed(){
        String message = message(code: "default.signup.failed")
        render(view:'/user/_login',model: [message:message])
    }

    def updateDetails() {
        String template = userService.generateUpdatePopup(params.userId)
        def map = [form: template]
        render map as JSON
    }

    def saveOTP(){
        params.OTP = userService.generateOTP()
        def response = userService.saveOTP(params)
        if(response != null){
            render(view: 'forgotPassword',model: [email:params.email])
            def link = grailsLinkGenerator.link(controller: 'user', action: 'passwordChange', absolute: true,params: [email:params.email])
            mailService.sendMail {
                multipart true
                to params.email
                subject message(code: 'default.reset.password')
                html "${message(code: 'default.password.reset')}"+"<strong>${params.OTP}<strong> <br> ${message(code: 'default.login.url')} " +
                        "<a href='${link}'>${message(code: 'default.click.here')} </a>."
            }
        }
        else{
            render(template: 'noUser')
        }
    }

    def forgotPassword(){
        String password = params.password
        params.password = password.encodeAsBase64()
        def user = userService.forgotPassword(params)
        if(user == null){
            flash.message = message(code: "default.password.changed.failed")
            redirect(controller: "user", action: "loginOtp")
        }
        else {
            flash.message = message(code: "default.password.changed")
            redirect(controller: "user", action: "login")
            mailService.sendMail {
                multipart true
                to params.email
                subject message(code: 'default.password.changed')
                body message(code: 'default.password.changed')
            }
        }
    }

    def logout(){
        render(view: '/user/_login')
    }

    def uploadFile(int secUserId) {
        String template = userService.generateUploadPopup(secUserId)
        def map = [form: template]
        render map as JSON
    }

    def upload() {
//        def f = request.getFile('myFile')
//        CommonsMultipartFile uploadedFile = params.myFile
//        params.myFile = uploadedFile.bytes
//        def documentInstance = new UploadFiles()
//        params.fileName = f.getOriginalFilename()
//        params.myFile = f.getBytes()
//        if (f.empty) {
//            flash.message = 'file cannot be empty'
//            render(view: '_uploadFile')
//            return
//        }
//        f.transferTo(new File('C:\\Users\\Ongraph Technologies\\Desktop\\upload file\\'+params.fileName))
//        userService.saveFile(params)
//        print("TATA"+params)
//        render(view: '_home')
        def file = request.getFile('myFile')
        if(file.isEmpty())
        {
            flash.message = "File cannot be empty"
            render(view: '_uploadFile')
            return
        }
        else
        {
            def documentInstance = new UploadFiles()
            documentInstance.fileName = file.getOriginalFilename()
            documentInstance.myFile = file.getBytes()
            documentInstance.secUser = SecUser.findById(params.secUserId)
            documentInstance.save()
            render(view: '_home')
        }
    }

    def OTPAuthenticate(){
        def user = userService.OTPAuthenticate(params)
        if(user == null){
            flash.message = message(code: "login.failed.otp")
            redirect(controller: "user", action: "loginOtp")
        }
        else {
            redirect(controller: "user", action: "home")
        }
    }

    def fetchFile(){
        SecUser user = SecUser.findById(params.secUser)
        def file = UploadFiles.findAllBySecUser(user)
        def fileName = file.fileName
        for(int i=0;i<fileName.size();i++){
            def fileNames = UploadFiles.findByFileName(fileName.get(i))
            byte[] myFile = fileNames.myFile
            response.contentType = myFile
            response.outputStream << myFile
            response.outputStream.flush()
        }
    }

//        def product = UploadFiles.findByFileName(params.secUser)
//        println("+Params"+product.class)
//        byte[] imageInByte = product.myFile
//        response.contentType = imageInByte
//        response.outputStream << imageInByte
//        response.outputStream.flush()

    def showFile(String secUser){
        String template = userService.generateFilePopUp(secUser)
        def map = [form: template]
        render map as JSON
    }
}
