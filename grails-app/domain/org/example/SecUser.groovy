package org.example

import grails_crud.UploadFiles
import org.hibernate.mapping.PrimaryKey

class SecUser {
    transient springSecurityService

    Integer id
    String firstName
    String lastName
    String email
    String password
    String dob
    String age
    String otp
    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    static constraints = {
        id size: 1..5, blank: false, unique: true
        firstName blank: false
        lastName blank: false
        email email: true, blank: false,unique: true
        age(nullable: true)
        password size: 5..40,blank: false
        otp(nullable: true)

    }

    static hasMany = [secUserSecRole:SecUserSecRole,uploadFiles: UploadFiles];

    static mapping = {
        version false
        password column: '`password`'
        secUserSecRole cascade: 'all-delete-orphan'
        uploadFiles cascade: 'all-delete-orphan'
    }

    Set<SecRole> getAuthorities() {
        SecUserSecRole.findAllBySecUser(this).collect { it.secRole } as Set
    }

//    def beforeInsert() {
//        encodePassword()
//    }
//
//    def beforeUpdate() {
//        if (isDirty('password')) {
//            encodePassword()
//        }
//    }
//
//    protected void encodePassword() {
//        password = springSecurityService.encodePassword(password)
//    }
}
