package grails_crud

import org.example.SecUser

class UploadFiles {

    static belongsTo = [secUser: SecUser]
    String fileName
    byte[] myFile

    static constraints = {
        secUser(nullable: true)
        fileName(nullable: true)
        myFile(blank:true,nullable: true,size:0..100000000)
    }
}
