package grails_crud


import grails_crud.dto.DataTableDTO
import grails_crud.util.DateUtil
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.example.SecRole
import org.example.SecUser
import org.example.SecUserSecRole
import org.springframework.transaction.annotation.Transactional

@Transactional
class UserService {

    def groovyPageRenderer

    def saveData(GrailsParameterMap params) {
        SecUser user = new SecUser(params)
        if (user.validate()) {
            def role = SecRole.findByAuthority("ROLE_USER")
            user.save(flush: true)
            SecUserSecRole.create(user,role)
            return user
        }
        else {
            return null
        }
    }

    def authentication(GrailsParameterMap params) {
        def email = params.email
        def password = params.password
        def user = SecUser.findByEmailAndPassword(email,password)
        if(user){
            return user
        }
        else{
            return  null
        }
    }

    def getDataById(Serializable id) {
        return SecUser.get(id)
    }

    def updateDataById(GrailsParameterMap params) {
        def user = SecUser.get(params.id)
        user.properties = params
        if (user.validate()) {
            user.save(flush: true)
        }
        return user
    }

    def deleteUser(GrailsParameterMap params){
        def user = SecUser.get(params.id)
        user.properties = params
        if(user != null){
            user.delete(flush: true)
        } else {

        }
    }

    def generateSignupPopup() {
        String template = groovyPageRenderer.render(view: '/user/signup')
        return template
    }

    DataTableDTO<List<List<String>>> LoadData() {
        List<SecUser> userList = SecUser.getAll()
        List<List<String>> teamData = []
            List<String> dataRow = []
            for(SecUser users : userList){
                dataRow << users.firstName
                dataRow << users.lastName
                dataRow << users.email
                dataRow << users.age
                dataRow << DateUtil.formatYearMonthDayToDayMonthYear(users.dob)
                dataRow << users.id
                teamData << dataRow
                dataRow = []
            }
        return new DataTableDTO<List<List<String>>>(data: teamData)
    }

    def generateUpdatePopup(Serializable id) {
        def response = SecUser.get(id)
//        byte[] decodeArr = (response.password).decodeBase64()
//        response.password = new String(decodeArr)
        String template = groovyPageRenderer.render(template: '/user/updateDetails', model: [details:response])
        return template
    }

    def saveOTP(GrailsParameterMap params){
        def user = SecUser.findByEmail(params.email)
        if(user != null){
            user.otp = params.OTP
            user.properties = params
            if (user.validate()) {
                user.save(flush: true)
            }
            return user
        }
        else{
            return null
        }
    }

    def forgotPassword(GrailsParameterMap params){
        def otp = params.otp
        def email = params.email
        def user = SecUser.findByEmailAndOtp(email,otp)
        if(user && user.password != params.password){
            user.password = params.password
            user.properties = params
            user.save(flush: true)
            return user
        }
        else{
            return  null
        }
    }

    def OTPAuthenticate(GrailsParameterMap params){
        def otp = params.otp
        def email = params.email
        def user = SecUser.findByEmailAndOtp(email,otp)
        if(user){
            return user
        }
        else{
            return  null
        }
    }

    def generateUploadPopup(secUserId) {
        String template = groovyPageRenderer.render(template: '/user/uploadFile',model: [secUserId:secUserId])
        return template
    }

//    def saveFile(GrailsParameterMap params){
//        params.secUser = SecUser.findById(params.secUserId)
//        UploadFiles files = new UploadFiles(params)
//        files.myFile = params.myFile
//        println("params"+params)
//        if (files.validate()) {
//            files.save(flush: true)
//            return files
//        }
//        else {
//            return null
//        }
//    }

    def generateOTP(){
        String numbers = "0123456789";
        Random rndm_method = new Random();
        char[] OTP = new char[4];
        for (int i = 0; i < 4; i++)
        {
            OTP[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return OTP.toString()
    }

    def generateFilePopUp(secUser) {
        String template = groovyPageRenderer.render(view: '/user/showUserFiles',model: [secUser:secUser])
        return template
    }
}
