<html>
<head>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'styling.css')}" type="text/css">
    <title>Log In</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
            integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
            integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
            crossorigin="anonymous"></script>
    <script>
        $(document).on('click', '#signup', function () {
            $.ajax({
                url: "${g.createLink(controller: 'user', action: 'signup')}",
                dataType: 'json',
                success: function (response) {
                    $('#signupPopup').html(response.form);
                    $('#signupDetails').modal('show');
                }
            });
        });
    </script>

</head>

<body class="bg-secondary bg-opacity-50">
<div class="modal fade" id="signupDetails" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" id="signupPopup">
    </div>
</div>

<div class="container mt-2">
    <div class="card mt-5 login shadow-lg p-5 mb-5 bg-white rounded">
        <div class="mt-2 text-center">
        </div>
        <g:form controller="user" action="authenticate" method="POST">
            <h3 class="text-center mt-3 mb-3 fst-italic text-uppercase">Log In</h3>

            <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic">
                <g:message class="form-label mb-3" code="default.email" default="Email Id :"/>
                <g:textField name="email" class="login_email form-control"/>
            </div>

            <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic">
                <g:message class="form-label" code="default.password" default="Password :"/>
                <g:passwordField name="password" class="login_password form-control"/>
            </div>

            <g:link controller="user" action="loginOtp">Forgot Password?</g:link>

            <g:if test="${flash.message}">
                <div class="message fw-bold text-start text-danger fst-italic text-uppercase">${flash.message}</div>
            </g:if>

            <g:if test="${message}">
                <div class="message text-start text-danger fst-italic"><g:message code="default.signup.failed"/></div>
            </g:if>

            <div class="container text-center mb-2 mt-5">
                <button type="submit" class="btn btn-outline-success text-uppercase">Log In</button>
            </div>
        </g:form>
        <div class="container text-center mb-3">
            <button class="btn btn-outline-primary text-uppercase" id="signup">Sign Up</button>
        </div>

%{--        <g:form controller="user" action="loginOtp">--}%
%{--            <div class="container text-center mb-2 mt-3">--}%
%{--                <button class="btn btn-outline-secondary text-uppercase" id="otp">Log In Using OTP</button>--}%
%{--            </div>--}%
%{--        </g:form>--}%

    </div>
</div>
</body>
</html>