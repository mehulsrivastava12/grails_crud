<html>
<head>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'styling.css')}" type="text/css">

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
          integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
          crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
          integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
          crossorigin="anonymous"></script>
</head>

<body class="bg-secondary bg-opacity-50">
<div class="container mt-5">
  <div class="card mt-5 login shadow-lg p-5 mb-5 bg-white rounded">
    <div class="mt-2 text-center">
      <g:form controller="user" action="forgotPassword" method="POST">
        <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic text-start">
          <g:message class="form-label mb-3" code="default.otp" default="Enter OTP :"/>
          <g:textField name="otp" class="form-control"/>
        </div>

        <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic text-start">
          <g:message class="form-label mb-3" code="default.new.password" default="New Password :"/>
          <g:passwordField name="new_password" id="new_password" class="form-control"/>
        </div>

        <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic text-start">
          <g:message class="form-label mb-3" code="default.confirm.password" default="Confirm Password :"/>
          <g:passwordField name="password" id="password" class="form-control"/>
        </div>


        <g:hiddenField name="email" value="${email}"/>
        <div class="container text-center mb-2 mt-5">
          <button type="submit" class="btn btn-outline-success text-uppercase" onclick="submit()">Submit</button>
        </div>
      </g:form>
    </div>
  </div>
</div>
</body>
</html>