<%--
  Created by IntelliJ IDEA.
  User: Ongraph Technologies
  Date: 7/21/2022
  Time: 3:02 PM
--%>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><g:message code="default.upload.label" default="Upload File"/></h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="form">
            %{--            <g:form controller="user" method="post" action="upload" enctype="multipart/form-data">--}%
            <div class="modal-body">
                <div>
                    <h4>File Uploaded Successfully</h4>

                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" onclick="submit()"><g:message code="default.submit.label" default="Submit" /></button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
            </div>
            %{--            </g:form>--}%
        </div>
    </div>
</div>