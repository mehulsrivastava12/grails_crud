<html>
<head>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'styling.css')}" type="text/css">
  <title>Log In</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js"
          integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"
          crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js"
          integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK"
          crossorigin="anonymous"></script>
%{--  <script>--}%

%{--    function generateOTP() {--}%
%{--      var digits = '0123456789';--}%
%{--      let OTP = '';--}%
%{--      for (let i = 0; i < 4; i++ ) {--}%
%{--        OTP += digits[Math.floor(Math.random() * 10)];--}%
%{--      }--}%
%{--      var email = $('#email').val()--}%
%{--      $.ajax({--}%
%{--        type: "GET",--}%
%{--        data: {--}%
%{--          OTP:OTP,--}%
%{--          email:email--}%
%{--        },--}%
%{--        url: "${g.createLink(controller: 'user',action: 'saveOTP')}",--}%
%{--        success: function (response){--}%
%{--          $('#otpPopup').html(response);--}%
%{--          $('#otpDetails').modal('show');--}%
%{--        },--}%
%{--        error: function (){--}%

%{--        }--}%
%{--      });--}%
%{--    }--}%

%{--  </script>--}%

</head>

<body class="bg-secondary bg-opacity-50">
<div class="modal fade" id="otpDetails" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document" id="otpPopup">
  </div>
</div>

<div class="container mt-2">
  <div class="card mt-5 login_otp shadow-lg p-5 mb-5 bg-white rounded">
    <div class="mt-2 text-center">
    </div>
    <g:form action="saveOTP">
      <h3 class="text-center mt-3 mb-3 fst-italic text-uppercase">Forgot Password</h3>

      <div class="form-group mb-3 mt-5 ml-5 mr-5 fw-bold fs-5 text-uppercase fst-italic">
        <g:message class="form-label mb-3" code="default.email" default="Email Id :"/>
        <g:textField name="email"  id="email" class="login_email form-control" required="required"/>
      </div>

      <g:if test="${flash.message}">
        <div class="message text-start text-danger fst-italic text-uppercase">${flash.message}</div>
      </g:if>

      <div class="container text-center mb-2 mt-5">
        <button type="submit" class="btn btn-outline-success text-uppercase">Generate OTP</button>
      </div>
    </g:form>
  </div>

%{--      <g:form controller="user" action="login">--}%
%{--        <div class="container text-center mb-2 mt-3">--}%
%{--          <button type="submit" class="btn btn-outline-primary text-uppercase">Log In Using Password</button>--}%
%{--        </div>--}%
%{--      </g:form>--}%

  </div>
</div>
</body>
</html>