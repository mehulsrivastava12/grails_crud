<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
            crossorigin="anonymous"
    ></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css"/>

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>

%{--    <script--}%
%{--            src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js"--}%
%{--            integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg=="--}%
%{--            crossorigin="anonymous"--}%
%{--            referrerpolicy="no-referrer"--}%
%{--    ></script>--}%
    <title></title>
</head>

<body>
<div class="modal fade" id="PopupDetails" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document" id="addPopup">
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
%{--            <li class="nav-item">--}%
%{--                <button class="btn btn-outline-secondary btn-lg mr-5 ml-5" id="upload" type="submit"><g:message code="default.upload.label" default="Upload Files"/></button>--}%
%{--            </li>--}%
            <li class="nav-item">
                <div class="float-right ml-5">
                    <g:form class="form-inline my-2 my-lg-0" action="logout">
                        <button class="btn btn-outline-info btn-lg" type="submit"><g:message code="default.logout" default="Logout"/></button>
                    </g:form>
                </div>
            </li>
        </ul>

    </div>
</nav>

%{--<g:form enctype='multipart/form-data' method='POST' action='upload' controller='user'>--}%
%{--    <input type='file' name='userFiles[]' multiple />--}%
%{--    <button type='submit'>Submit</button>--}%
%{--</g:form>--}%
<div>
    <div class="mt-5">
        <div class="ml-5 mr-5">
            <table id="getALlUser" class="table table-bordered table-hover">
                <thead class="thead-dark">
                <tr>
                    <th width="100px"><g:message code="default.firstName" default="First Name"/> </th>
                    <th width="100px"><g:message code="default.lastName" default="Last Name"/></th>
                    <th width="100px"><g:message code="default.email" default="Email Id"/></th>
                    <th width="100px"><g:message code="default.age" default="Age"/></th>
                    <th width="100px"><g:message code="default.dob" default="Date Of Birth"/></th>
                    <th width="100px"><g:message code="default.update" default="Update"/></th>
                    <th width="100px"><g:message code="default.delete" default="Delete"/></th>
                    <th width="100px"><g:message code="default.upload.label" default="Upload Files"/></th>
                    <th width="100px"><g:message code="default.show.uploaded.label" default="Show Upload Files"/></th>
                </tr>
                </thead>
                </table>
            </div>
    </div>
</div>

<script>

    $("#getALlUser").DataTable({
        paging : true,
        ajax: {
            type: "GET",
            url: "${g.createLink(controller: 'user',action: 'fetchAllUser')}"
        },
        language: {searchPlaceholder: "Search User",
        },
        autoWidth: true,
        dom: "<'dt-toolbar'fr>t<'dt-toolbar-header'<ip>>",
        pagingType: "simple_numbers",
        pageLength : 2,
        deferRender: true,
        aaSorting: [[1, "desc"]],
        columnDefs: [
            {
                targets: 0,
                render: function (row, type, val, meta) {
                    return val[0];
                }
            },
            {
                targets: 1,
                render: function (row, type, val, meta) {
                    return val[1];
                }
            },
            {
                targets: 2,
                render: function (row, type, val, meta) {
                    return val[2];
                }
            },
            {
                targets: 3,
                render: function (row, type, val, meta) {
                    return val[3];
                }
            },
            {
                targets: 4,
                render: function (row, type, val, meta) {
                    return val[4];
                }
            },
            {
                targets: 5,
                // defaultContent: "<input id='btnDetails' class='btn btn-success mr-5' width='25px' value='Update' />" +
                //      "<input id='btnDetails' class='btn btn-danger' width='25px' onclick='getConfirmation()' value='Delete' />"
                render: function (row, type, val, meta) {
                    var span = document.createElement("span");
                    var button = document.createElement("input");
                    button.setAttribute("type", "button");
                    button.setAttribute("class", "btn btn-success Update");
                    button.setAttribute("value", "Update")
                    button.setAttribute("id","update")
                    button.setAttribute("onclick", "updateById("+val[5]+")")
                    span.appendChild(button);
                    return span.outerHTML;
                }
            },
            {
                targets: 6,
                render: function (row, type, val, meta) {
                    var span = document.createElement("span");
                    var button = document.createElement("input");
                    button.setAttribute("type", "button");
                    button.setAttribute("class", "btn btn-danger");
                    button.setAttribute("value", "Delete")
                    button.setAttribute("onclick", "getConfirmation("+val[5]+")")
                    span.appendChild(button);
                    return span.outerHTML;
                }
            },
            {
                targets: 7,
                render: function (row, type, val, meta) {
                    var span = document.createElement("span");
                    var button = document.createElement("input");
                    button.setAttribute("type", "submit");
                    button.setAttribute("class", "btn btn-outline-secondary btn-lg mr-5 ml-5");
                    button.setAttribute("value", "Upload")
                    button.setAttribute("onclick", "uploadById("+val[5]+")")
                    span.appendChild(button);
                    return span.outerHTML;
                }
            },
            {
              target:8,
                render: function (row, type, val, meta) {
                    var span = document.createElement("span");
                    var button = document.createElement("input");
                    button.setAttribute("type", "submit");
                    button.setAttribute("class", "btn btn-outline-secondary btn-lg mr-5 ml-5");
                    button.setAttribute("value", "Show Files")
                    button.setAttribute("onclick", "showFile("+val[5]+")")
                    span.appendChild(button);
                    return span.outerHTML;
                }
            },
        ]
    });

    function getConfirmation(id) {
        var retVal = confirm("Are you sure you want to delete ?");
        if( retVal == true ){
            window.location = "/grails_crud/user/deleteUser/"+id;
        }
        else{
            return false;
        }
    }

    function updateById(id){
            $.ajax({
                url: "${g.createLink(controller: 'user', action: 'updateDetails')}",
                dataType: 'json',
                data: {
                    userId: id,
                },
                success: function (response) {
                    $('#addPopup').html(response.form);
                    $('#PopupDetails').modal('show');
                }
            });
        // window.location = "/grails_crud/user/getDataById/"+id;
    }

    function uploadById(id){
        $.ajax({
            url: "${g.createLink(controller: 'user', action: 'uploadFile')}",
            dataType: 'json',
            data:{
                secUserId:id,
            },
            success: function (response){
                $('#addPopup').html(response.form);
                $('#PopupDetails').modal('show');
            }
        });
    };

    function showFile(id){
        console.log(id)
        %{--<img src="${createLink(controller: 'user', action: 'fetchFile', params: ['secUser': id])}"/>--}%
        $.ajax({
            url: "${g.createLink(controller: 'user', action: 'showFile')}",
            dataType: 'json',
            data:{
                secUser:id,
            },
            success: function (response) {
                $('#addPopup').html(response.form);
                $('#PopupDetails').modal('show');
            }
        });
    };
</script>

<link rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
      crossorigin="anonymous">

<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
      integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
      crossorigin="anonymous" referrerpolicy="no-referrer" />

<script
        src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

<!-- CSS only -->
<link
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
        rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
        crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
        crossorigin="anonymous"></script>
</body>
</html>

