<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><g:message code="default.upload.label" default="Upload File"/></h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="form">
            <g:form controller="user" method="post" action="upload" enctype="multipart/form-data">
                <div class="modal-body">
                    <g:hiddenField name="secUserId" value="${secUserId}"/>
                    <div class="modal fade" id="uploadDetails" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document" id="uploadPopup">
                        </div>
                    </div>

                    <div>

                        <label>
                            Upload File
                        </label>
                        <input type="file" name="myFile" />

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><g:message code="default.submit.label" default="Submit" /></button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
                </div>
            </g:form>
        </div>
    </div>
</div>



