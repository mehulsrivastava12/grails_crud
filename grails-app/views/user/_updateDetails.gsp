<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title text-uppercase fst-italic" id="exampleModalLongTitle">Update Details</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body">

        <g:form controller="user" action="updateDataById" id="${details.id}"  method="POST">
            <div class="form-group mt-5 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="firstName" code="default.firstName" default="First Name :"/>
                <g:textField name="firstName" value="${details.firstName}" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="lastName" code="default.lastName" default="Last Name :"/>
                <g:textField name="lastName" value="${details.lastName}" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="email" code="default.email" default="Email Id :"/>
                <g:textField name="email" value="${details.email}" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="dob" code="default.dob" default="Date Of Birth :"/>
                <g:field name="dob"  class="form-control signup_date_width" value="${details.dob}"
                         placeholder="Enter Date Of Birth" type="date" id="dob"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="age" code="default.age" default="Age :"/>
                <g:select from="${1..100}" name="age" value="${details.age}" class="form-control"/>
            </div>

%{--            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">--}%
%{--                <g:message class="form-label" for="password" code="default.password" default="Password :"/>--}%
%{--                <g:textField name="password" value="${details.password}" class="form-control"/>--}%
%{--            </div>--}%

            <div class="container text-center mb-3">
                <button type="submit" class="btn btn-outline-success">Update</button>
            </div>

        </g:form>
    </div>
</div>