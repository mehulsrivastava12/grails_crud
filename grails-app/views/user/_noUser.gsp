<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title text-uppercase fst-italic text-warning" id="exampleModalLongTitle">ERROR</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <h4 class="text-secondary fw-bold fs-5 text-uppercase fst-italic">No User With This Email Exist</h4>
    </div>
</div>