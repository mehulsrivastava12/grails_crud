<%--
  Created by IntelliJ IDEA.
  User: Ongraph Technologies
  Date: 8/10/2022
  Time: 2:57 PM
--%>

%{--<%@ page contentType="text/html;charset=UTF-8" %>--}%
%{--<html>--}%
%{--<head>--}%
%{--    <title></title>--}%
%{--</head>--}%

%{--<body>--}%
%{--<img src="${createLink(controller: 'user', action: 'fetchFile', params: ['secUser': secUser])}"/>--}%
%{--<h4>HIII</h4>--}%
%{--</body>--}%
%{--</html>--}%

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><g:message code="default.upload.label" default="Upload File"/></h4>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="form">
%{--            <g:form controller="user" method="post" action="upload" enctype="multipart/form-data">--}%
                <div class="modal-body">
                    <g:hiddenField name="secUser" value="${secUser}"/>
                    <div class="modal fade" id="uploadDetails" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document" id="uploadPopup">
                        </div>
                    </div>
                    <div>
                        <h4>${res}</h4>
%{--                        <g:--}%
%{--                        <g:each in="${files}" var ="p">--}%
%{--                            <li>${p}</li>--}%
%{--                        </g:each>--}%
%{--                        <embed src="${createLink(controller: 'user', action: 'fetchFile', params: [secUser: secUser])}" style="width: 700px;"/>--}%
                    </div>
                    <img src="${createLink(controller: 'user', action: 'fetchFile', params: [secUser: secUser])}"/>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><g:message code="default.submit.label" default="Submit" /></button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><g:message code="default.close.label" /></button>
                </div>
%{--            </g:form>--}%
        </div>
    </div>
</div>