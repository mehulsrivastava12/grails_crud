
<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title text-center text-uppercase fst-italic" id="exampleModalLongTitle">Sign Up</h5>
        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <div class="modal-body">

        <g:form controller="user" action="create">
            <div class="form-group mt-5 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="firstName" code="default.firstName" default="First Name :"/>
                <g:textField id="firstName" name="firstName" required="required" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="lastName" code="default.lastName" default="Last Name :"/>
                <g:textField id="lastName" name="lastName"  required="required" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="email" code="default.email" default="Email Id :"/>
                <g:textField id="email" name="email"  required="required" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="dob" code="default.dob" default="Date Of Birth :"/>
                <g:field name="dob"  class="form-control signup_date_width"  required="required"
                        type="date" id="dob"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="age" code="default.age" default="Age :"/>
                <g:select from="${1..100}" id="age" name="age"  required="required" class="form-control"/>
            </div>

            <div class="form-group mt-4 ml-5 mr-5 fw-bold fs-6 text-uppercase fst-italic">
                <g:message class="form-label" for="password" code="default.password" default="Password :"/>
                <g:passwordField id="password" name="password" minlength="5" required="required" class="form-control"/>
            </div>

            <div class="container text-center mb-3 mt-3">
                <button type="submit" class="btn btn-outline-success">Sign Up</button>
            </div>

        </g:form>
</div>
</div>

